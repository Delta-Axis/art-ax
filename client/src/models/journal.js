export class Journal {
  _id = null;
  title = ""
  content = "";
  userId = null;
  datePosted = null;

  constructor(_id, title, content, userId, datePosted){
    this._id = _id;
    this.title = title;
    this.content = content;
    this.userId = userId;
    this.datePosted = datePosted;
  }
}