export class User {
  _id = null;
  firstName = "";
  lastName = "";
  username = "";
  email = "";
  birthDate = "";
  gender = "";
  bio = "";

  constructor(_id, firstName, lastName, username, email, birthDate, gender, bio) {
    this._id = _id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
    this.email = email;
    this.birthDate = birthDate;
    this.gender = gender;
    this.bio = bio;
    
  }
}