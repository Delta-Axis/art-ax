export class Artwork {
  _id = null;
  filePath = "";
  title = "";
  description = "";
  tags = "";
  datePosted = null;
  lastModified = null;
  userId = null;

  constructor(_id, filePath, title, description, tags, datePosted, lastModified, userId){
    this._id = _id;
    this.filePath = filePath;
    this.title = title;
    this.description = description;
    this.tags = tags;
    this.datePosted = datePosted;
    this.lastModified = lastModified;
    this.userId = userId;
  }
}