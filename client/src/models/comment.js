export class Comment {
  _id = null;
  artworkId = null;
  userId = null;
  comment = "";
  datePosted = null;

  constructor(_id, artworkId, userId, comment, datePosted) {
    this._id = _id;
    this.artworkId = artworkId;
    this.userId = userId;
    this.comment = comment;
    this.datePosted = datePosted;
  }
}