import React from "react";
import { ApiContainer } from './api.services';
import { Provider, Subscribe } from "unstated";
import { Account } from "models"

// Browser History
import history from 'History';

export class UserAuthContainer extends ApiContainer {
  api_schema = "auth"

  constructor({initialState = {}, saveState = true, service = "auth"}) {
    if (Object.keys(initialState).length == 0)
      initialState = new Account();
      
    super({ initialState: initialState, saveState: saveState, service: service });
  }

  async login(username = "", password = "", redirect = null, component = null) {
    if (username != "" && password != "") {
      password = Buffer.from(password).toString("base64");
      this.post({ username: username, password: password })
        .then((response) => {
          console.log(response);
          if (response.data._id) {
            this.setState(response.data, () => {
              console.log("Login Successful");
              if (redirect) history.replace(redirect);
            });
          } 
          
          else {
            if (component) {
              component.setState({ error: true }, () => {
                console.log("Invalid Credentials");
              });
            }
          }
        });
    } else {
      console.log("lol you fucked up");
    }
  }

  async logout() {
    this.redirect();

    console.log(`${this.state.username} has logged out`);
    for (let key of Object.keys(this.state)) {
      this.setState({ [key]: key == "username" ? 
          "guest" 
        : key == "role" ? 
          "" 
          : null 
      });
    }
  }

  async redirect() {
    if (/^\/user\/$/.test(location.pathname))
      history.replace(`/user/${this.state._id}`);
    if (location.pathname.startsWith("/submit"))
      history.replace("");
  }

  async register(firstName, lastName, username, email, password, birthDate, gender, component = null, user_role = "user") {
    let newUserData = {
      firstName: firstName,
      lastName: lastName,
      username: username,
      email: email.toLowerCase(),
      password: Buffer.from(password).toString("base64"),
      birthDate: new Date(birthDate),
      gender: gender.toLowerCase(),
      user_role: user_role.toLowerCase(),
    }
    super.create(`${this.api_schema}/create`, newUserData)
      .then((response) => {
        if (response.data && response.data.username) {
          console.log("registration data");
          console.log(response.data);
          /* if (component) // might not use this...
            component.setState({ registered: true }); */
            this.login(response.data.username, password, "/user/");
        }
      });
  }

  get() {
    return super.get(this.api_schema);
  }

  get(urlParam) {
    return super.get(this.api_schema, urlParam);
  }

  post(data = {}) {
    return super.post(this.api_schema, data);
  }

  create(data = {}) {
    return super.create(this.api_schema, data);
  }

  update(data = {}) {
    return super.update(this.api_schema, data);
  }

  delete(data = {}) {
    return super.delete(this.api_schema, data);
  }
}

const UserAuthService = new UserAuthContainer({});

export const UserAuthProvider = props => {
  return <Provider inject={props.inject || [UserAuthService]}>{props.children}</Provider>;
};

export const UserAuthSubscriber = props => {
  return <Subscribe to={props.to || [UserAuthService]}>{props.children}</Subscribe>;
};

export default UserAuthService;