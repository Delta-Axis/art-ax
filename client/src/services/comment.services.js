import React from "react";
import { ApiContainer } from "./api.services";
import { Provider, Subscribe } from "unstated";
import { Comment } from "models";

export class CommentContainer extends ApiContainer {
  api_schema = "comment";
  comments = [];

  constructor({ initialState = {}, saveState = true, service = "comment" }) {
    if (Object.keys(initialState).length == 0) initialState = new Comment();

    super({
      initialState: initialState,
      saveState: saveState,
      service: service
    });
  }

  async getArtworkComments(artworkId) {
    this.post({ artworkId: artworkId }).then(response => {
      console.log(repsonse.data);
      if (Object.keys(response.data).length > 1) {
        this.setState(response.data);
      }
      comments.push(this.state);
    });
  }

  async getJournalComments(journalId) {
    this.post({ journalId: journalId }).then(response => {
      console.log(response.data);
      if (Object.keys(response.data).length > 1) {
        this.setState(response.data);
      }
      comments.push(this.state);
    });
  }

  get() {
    return super.get(this.api_schema);
  }

  get(urlParam) {
    return super.get(this.api_schema, urlParam);
  }

  post(data = {}) {
    return super.post(this.api_schema, data);
  }

  create(data = {}) {
    return super.create(this.api_schema, data);
  }

  update(data = {}) {
    return super.update(this.api_schema, data);
  }

  delete(data = {}) {
    return super.delete(this.api_schema, data);
  }
}

const CommentService = new CommentContainer({ saveState: false });

export const CommentServiceProvider = props => {
  return (
    <Provider inject={props.inject || [CommentService]}>
      {props.children}
    </Provider>
  );
};

export const CommentServiceSubscriber = props => {
  return (
    <Subscribe to={props.to || [CommentService]}>{props.children}</Subscribe>
  );
};

export default CommentService;
