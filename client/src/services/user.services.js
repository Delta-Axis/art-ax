import React from "react";
import { ApiContainer } from './api.services';
import { Provider, Subscribe } from "unstated";
import { User } from "models"

export class UserContainer extends ApiContainer {
  api_schema = "users"

  constructor({initialState = {}, saveState = true, service = "user"}) {
    if (Object.keys(initialState).length == 0)
      initialState = {
        user: new User(),
        users: []
      }

    super({ initialState: initialState, saveState: saveState, service: service });
  }

  async setDocumentTitle(user) {
    let page = `${user.username ? `${user.username}'s` : `User's`} Profile`;
    if (document.title.includes(" | ")) {
      let title = document.title.split(" | ");
      title[title.length - 1] = page;
      document.title = title.join(" | ");
    } else {
      document.title = `${document.title} | ${page}`;
    }
  }

  async getUser(userId, component = null) {
    this.setState({ user: new User() });
    this.post({ userId: userId }).then((response) => {
        if (Object.keys(response.data).length > 1) {
          this.setState({ user: response.data }, () => {
            if (location.pathname.includes("/user/"))
              this.setDocumentTitle(this.state.user);
            if (component)
              component.setState(component.state);
          });
        }
      });
  }

  async getUserByUsername(username, component = null) {
    this.setState({ user: new User() });
    this.post({ username: username }).then((response) => {
        if (Object.keys(response.data).length > 1) {
          this.setState({ user: response.data }, () => {
            if (location.pathname.includes("/user/"))
              this.setDocumentTitle(this.state.user);
            if (component)
              component.setState(component.state);
          });
        }
      });
  }

  async getUsers(component = null) {
    this.setState({ users: [] });
    this.get().then((response) => {
        if (response.data.length) {
          this.setState({ users: response.data }, () => {
            if (component)
              component.setState(component.state);
          });
        }
      });
  }

  get() {
    return super.get(this.api_schema);
  }

  get(urlParam) {
    return super.get(this.api_schema, urlParam);
  }

  post(data = {}) {
    return super.post(this.api_schema, data);
  }

  create(data = {}) {
    return super.create(this.api_schema, data);
  }

  update(data = {}) {
    return super.update(this.api_schema, data);
  }

  delete(data = {}) {
    return super.delete(this.api_schema, data);
  }
}

const UserService = new UserContainer({ saveState: false });

export const UserServiceProvider = props => {
  return <Provider inject={props.inject || [UserService]}>{props.children}</Provider>;
};

export const UserServiceSubscriber = props => {
  return <Subscribe to={props.to || [UserService]}>{props.children}</Subscribe>;
};

export default UserService;