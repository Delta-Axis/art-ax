import React from "react";
import { ApiContainer } from "./api.services";
import { Provider, Subscribe } from "unstated";
import { Artwork } from "models";
import history from "History";

export class ArtworkContainer extends ApiContainer {
  api_schema = "art";
  
  constructor({ initialState = {}, saveState = true, service = "artwork" }) {
    if (Object.keys(initialState).length == 0) 
      initialState = {
        artwork: new Artwork(),
        artworks: []
      }

    super({
      initialState: initialState,
      saveState: saveState,
      service: service
    });
  }

  async getArtwork(artworkId) {
    this.setState({ artwork: new Artwork() });
    this.post({ artworkId: artworkId }).then((response) => {
      if (Object.keys(response.data).length > 1)
        this.setState({ artwork: response.data });
    });
  }

  async getArtworks(component = null) {
    this.setState({ artworks: [] });
    this.get().then((response) => {
      if (response.data.length) {
        this.setState({ artworks: Object.values(response.data) }, () => {
          if (component)
            component.setState(component.state);
        });
      }
    });
  }

  async getArtworksByUserId(userId, component = null) {
    this.setState({ artworks: [] });
    this.post({ userId: userId }).then(response => {
      if (response.data.length) {
        this.setState({ artworks: Object.values(response.data) }, () => {
          if (component)
            component.setState(component.state);
        });
      }
    });
  }

  async upload(data = { }, token, component = null) {
    // api_schema, data, requireAuth, authToken
    super.create(`${this.api_schema}/upload`, data, true, token)
      .then((response) => {
        let data = response.data;
        if (component && !data.uploaded) {
          // do something
          component.setState({ uploaded: response.data.uploaded })
        }
        if (data.artwork) {
          this.setState({ artwork: response.data.artwork }, () => {
            history.push(`/art/${this.state.artwork._id}`);
          });
        }
        delete data.artwork;
        console.log(data);
      });
  }

  get() {
    return super.get(this.api_schema);
  }

  get(urlParam) {
    return super.get(this.api_schema, urlParam);  
  }

  post(data = {}) {
    return super.post(this.api_schema, data);
  }

  create(data = {}) {
    return super.create(this.api_schema, data);
  }

  update(data = {}) {
    return super.update(this.api_schema, data);
  }

  delete(data = {}) {
    return super.delete(this.api_schema, data);
  }
}

const ArtworkService = new ArtworkContainer({ saveState: false });

export const ArtworkServiceProvider = props => {
  return (
    <Provider inject={props.inject || [ArtworkService]}>
      {props.children}
    </Provider>
  );
};

export const ArtworkServiceSubscriber = props => {
  return (
    <Subscribe to={props.to || [ArtworkService]}>{props.children}</Subscribe>
  );
};

export default ArtworkService;
