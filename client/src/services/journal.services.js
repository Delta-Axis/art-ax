import React from "react";
import { ApiContainer } from "./api.services";
import { Provider, Subscribe } from "unstated";
import { Journal } from "models";

export class JournalContainer extends ApiContainer {
  api_schema = "journal";
  journals = [];

  constructor({ initialState = {}, saveState = true, service = "journal" }) {
    if (Object.keys(initialState).length == 0) initialState = new Journal();

    super({
      initialState: initialState,
      saveState: saveState,
      service: service
    });
  }

  async getJournals() {
    this.get().then(response => {
      console.log(response.data);
      if (Object.keys(response.data).length > 1) {
        this.setState(response.data);
      }
      journals.push(this.state);
    });
  }

  async getUserJournals(userId) {
    this.post({ userId: userId }).then(response => {
      console.log(response.data);
      if (Object.keys(response.data).length > 1) {
        this.setState(response.data);
      }
      journals.push(this.state);
    });
  }

  get() {
    return super.get(this.api_schema);
  }

  get(urlParam) {
    return super.get(this.api_schema, urlParam);
  }

  post(data = {}) {
    return super.post(this.api_schema, data);
  }

  create(data = {}) {
    return super.create(this.api_schema, data);
  }

  update(data = {}) {
    return super.update(this.api_schema, data);
  }

  delete(data = {}) {
    return super.delete(this.api_schema, data);
  }
}

const JournalService = new JournalContainer({ saveState: false });

export const JournalServiceProvider = props => {
  return (
    <Provider inject={props.inject || [JournalService]}>
      {props.children}
    </Provider>
  );
};

export const JournalServiceSubscriber = props => {
  return (
    <Subscribe to={props.to || [JournalService]}>{props.children}</Subscribe>
  );
};

export default JournalService;
