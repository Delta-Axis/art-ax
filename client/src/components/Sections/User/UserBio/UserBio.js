import React, { Component } from 'react';
import { ContentBlock, Link } from 'Layout';
import { UserPicture, UserBioDetails } from 'Sections';
import './UserBio.css';

class UserBio extends Component {
  constructor(props) {
    super(props);
  }

  state = { }

  componentDidMount() {  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  render() {
    const { User } = this.props;
    let Header = { 
      firstName: "First Name",
      lastName: "Last Name",
      username: "Username" ,
    }
    if (User) {
      Header = {
        firstName: User.firstName,
        lastName: User.lastName,
        username: User.username,
      };
    }

    return (
      <ContentBlock className="user-bio">
        <ContentBlock className="user-bio-content d-flex" size="5">
          <UserPicture />
          <UserBioDetails Header={Header} Bio={User ? User.bio : "Hi! This is my biography."}/>
        </ContentBlock>
      </ContentBlock>
    );
  }
}

export { UserBio };