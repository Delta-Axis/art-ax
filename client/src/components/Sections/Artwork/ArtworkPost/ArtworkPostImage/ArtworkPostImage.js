import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import css from './ArtworkPostImage.css';

class ArtworkPostImage extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    ContentSize: this.props.ContentSize !== undefined ? Number.parseInt(this.props.ContentSize) : 10,
    Image: this.props.src !== undefined ? this.props.src : `/assets/images/example.jpg`,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  componentDidMount() { }

  render() {
    const { } = this.props;
    const { Image, ContentSize } = this.state;
    return (
      <ContentBlock className="artwork-post-image" size={ContentSize} style={{ backgroundImage: `url('${Image}')` }}>
        
      </ContentBlock>
    );
  }
}

export { ArtworkPostImage };