import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import { ArtworkPostEntry, CommentFeed } from 'Sections';
import css from './ArtworkPostDetails.css';

class ArtworkPostDetails extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    ContentSize: this.props.ContentSize !== undefined ? Number.parseInt(this.props.ContentSize) : 10,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  render() {
    const { Artwork } = this.props;
    const { ContentSize } = this.state;

    return (
      <ContentBlock className="artwork-post-details" size={ContentSize}>
        <ArtworkPostEntry Artwork={Artwork}/>
        <CommentFeed ArtworkID={Artwork._id}/>
      </ContentBlock>
    );
  }
}

export { ArtworkPostDetails };