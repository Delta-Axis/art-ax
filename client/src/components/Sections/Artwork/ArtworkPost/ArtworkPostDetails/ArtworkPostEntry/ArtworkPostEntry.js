import React, { Component } from 'react';
import { ContentBlock, Link } from 'Layout';
import { ArtworkPostHeader, ArtworkPostDescription } from 'Sections';
import './ArtworkPostEntry.css';

import { UserServiceProvider, UserServiceSubscriber, UserContainer } from 'services';

class ArtworkPostEntry extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    title: this.props.title !== undefined ? this.props.title : "The Great Wave off Kanagawa",
    artist: this.props.artist !== undefined ? this.props.artist : "Hokusai",
  }

  componentDidMount() {  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  render() {
    const { UserPicture, Artwork } = this.props;

    return (
      <UserServiceProvider>
        <UserServiceSubscriber>
          { user => (
            <ContentBlock className="artwork-post-entry">
              <ContentBlock className="artwork-post-entry-content d-flex">
                <ArtworkPostHeader 
                  service={{ user: user }}
                  Title={Artwork.title} 
                  ArtistID={Artwork.userId}
                  UserPicture={UserPicture}/>
                <ArtworkPostDescription 
                  Description={Artwork.description}/>
              </ContentBlock>
            </ContentBlock>
          )}
        </UserServiceSubscriber>
      </UserServiceProvider>
    );
  }
}

export { ArtworkPostEntry };