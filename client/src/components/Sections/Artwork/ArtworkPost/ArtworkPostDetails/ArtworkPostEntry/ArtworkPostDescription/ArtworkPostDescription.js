/*
ArtXperience Component
ArtworkPostDescription
*/
import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import './ArtworkPostDescription.css';

class ArtworkPostDescription extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    description: 
      this.props.Content !== undefined ? 
        this.props.Content 
      : `This print is a yoko-e, that is, a landscape format produced to the ōban size, about 25 cm high by 37 cm wide.\n
    \nThe composition comprises three main elements: the sea whipped up by a storm, three boats and a mountain. It includes the signature in the upper left-hand corner.`,
  }

  componentDidMount() { }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }
    
    return state;
  }

  render() {
    const { id, className, children, style, Description } = this.props;
    
    return (
      <ContentBlock id={id} style={style ? style : null}
        className={`artwork-post-description${className ? ` ${className}` : ``}`}>
        {Description.replace(/\\r\\n/g, "<br />")}
      </ContentBlock>
    );
  }
}

export { ArtworkPostDescription };