/*
ArtXperience Component
ArtworkPostHeader
*/
import React, { Component } from 'react';
import { ContentBlock, Link } from 'Layout';
import { UserPicture } from 'Sections';
import './ArtworkPostHeader.css';

class ArtworkPostHeader extends Component {
  constructor(props) {
    super(props);

    const { ArtistID } = this.props;
    const { service } = this.state;
    const { user } = service;

    if (user)
      user.getUser(ArtistID);
  }

  state = { 
    service: this.props.service ? this.props.service : null
  }

  componentDidMount() { }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }
    
    return state;
  }

  render() {
    const { id, className, children, style, Title, Avatar } = this.props;
    const { service } = this.state;
    const { user } = service;
    let User = user.state.user;
    
    return (
      User._id ?
        <ContentBlock className="artwork-post-header d-flex">
          <Link Url={`/user/${User._id}`}>
            <UserPicture Avatar={Avatar} Size="sm"/>
          </Link>
          <ContentBlock className="artwork-post-header-details" size="grow">
            <div className="artwork-post-artist"><Link Url={`/user/${User._id}`}>{User.username}</Link></div>
            <div className="artwork-post-title">{Title}</div>
          </ContentBlock>
        </ContentBlock>
      : null
    );
  }
}

export { ArtworkPostHeader };