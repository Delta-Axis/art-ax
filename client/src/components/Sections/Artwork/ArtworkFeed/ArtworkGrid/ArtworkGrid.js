import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import { ArtworkBlock } from 'Sections';
import './ArtworkGrid.css';

import { UserServiceProvider, UserServiceSubscriber, UserContainer } from 'services';

class ArtworkGrid extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    service: this.props.service ? this.props.service : null,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    if (prevState) {
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
    }

    return state;
  }

  componentDidMount() {
    const { UserID } = this.props;
    const { service } = this.state;

    if (service)
      if (location.pathname.startsWith("/user/"))
        service.getArtworksByUserId(UserID);
      else
        service.getArtworks();
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname
      || this.props.ContentSize !== prevProps.ContentSize) {
      const { UserID } = this.props;
      const { service } = this.state;

      if (service)
        if (location.pathname.startsWith("/user/"))
          service.getArtworksByUserId(UserID);
        else
          service.getArtworks();
    }
  }

  renderArtworks = (artworks) => {
    if (artworks)
      return artworks.map((artwork, index) => {
        const container = new UserContainer({ saveState: false });
        return (
          <UserServiceProvider inject={[container]}>
            <UserServiceSubscriber to={[container]}>
              {user => (
                <ArtworkBlock key={artwork._id} 
                  service={{ user: user }} artwork={artwork} 
                  UserID={artwork.userId}/>
              )}
            </UserServiceSubscriber>
          </UserServiceProvider>
        );
      });
    
    return null;
  }

  render = () => {
    const { className, children, size, UserID } = this.props;
    const { service } = this.state;
    let artworks = null;
    if (service)
      artworks = service.state.artworks;

    return (
      <ContentBlock className={`artwork-grid b-${size}${` ${className ? className : ""}`}`}>
        {children}
        {this.renderArtworks(artworks)}
      </ContentBlock>
    );
  }
}

export { ArtworkGrid };