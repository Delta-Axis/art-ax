/*
ArtXperience Component
ArtworkBlock
*/
import React, { Component } from 'React';
import { Link } from 'Layout';
import './ArtworkBlock.css';

class ArtworkBlock extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    src: '/assets/images/example.jpg',
    details: this.props.details !== undefined ? 
        this.props.details 
      : (!location.pathname.includes("/artwork/latest") ? true : false),
    size: this.props.size !== undefined ? Number.parseInt(this.props.size) : 1,
    service: this.props.service ? this.props.service : {
      user: null
    },
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    if (prevState) {
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
    }

    return state;
  }

  componentDidMount() {
    const { UserID } = this.props;
    const { service } = this.state;
    const { user } = service;

    if (user)
      user.getUser(UserID);
  }

  handleImageError = (e) => {
    e.target.style.width = "100%";
    e.target.classList.add("error");
  }
  
  render = () => {
    const { blur, id, className, artwork, UserID } = this.props;
    const { src, details, size, container, service } = this.state;
    const { user } = service;
    
    let User = null;
    if (user.state.user._id)
      User = user.state.user;

    return (
      User ?
        <div className={`artwork-block b-${size}`}>
          { details &&
            <div className="artwork-details-overlay">
              <Link Url={`/art/${artwork._id}`}>
                {blur &&<div className="artwork-details-backdrop" style={{backgroundImage: `url('/artworks/${artwork.filename}')`}}></div>}
                <div className="artwork-details-background"></div>
                <div className="artwork-title">{artwork.title ? artwork.title : "The Great Wave off Kanegawa"}</div>
              </Link>
              <Link className="artwork-artist" Url={`/user/${User._id}`}>{User.username ? User.username : "Hokusai"}</Link>
            </div>
          }
          <Link Url={`/art/${artwork._id}`}><img className="artwork-thumb" src={artwork.filename ? `/artworks/${artwork.filename}` : src} onError={this.handleImageError} /></Link>
        </div>
      : null
    );
  }
}

export { ArtworkBlock };