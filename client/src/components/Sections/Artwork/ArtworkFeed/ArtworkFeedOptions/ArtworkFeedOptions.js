/*

*/
import React, { Component } from 'react';
import { ContentBlock, Link } from 'Layout';
import './ArtworkFeedOptions.css';

import { UserAuthSubscriber } from 'services';

class ArtworkFeedOptions extends Component {
  constructor(props) {
    super(props);
  }

  state = { };

  componentDidMount() { 

  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  createHomeOptions = (loggedIn) => {
    const categories = ["trending" , "latest"];

    let links = [];
    for (let i = 0; i < categories.length; i++) {
      let classes = 
        location.pathname.includes(categories[i]) ? 
          `option selected`
        : `option`;

      let url = location.pathname.includes(categories[i]) ? 
          `` : `/artwork/${categories[i]}`;
      
      let onClick = location.pathname.includes(categories[i]) ?
        (e) => { e.preventDefault() } : null;
      
      if (i == 0 && location.pathname == "/") {
        url = ``;
        classes += " selected";
        onClick = (e) => { e.preventDefault() };
      }
      
      links.push(<Link className={classes} Url={url} onClick={onClick}>{categories[i]}</Link>);
    }

    // if (loggedIn)
    //   links = this.createUserHomeOptions(links);

    return links;
  }

  createUserHomeOptions = (links) => {
    let categories = ["following"]

    for (let category of categories) {
      let classes = 
        location.pathname.includes(category) ? 
          `option selected`
        : `option`;
      
      let url = `/artwork/${category}`;
      
      links.push(<Link className={classes} Url={url}>{category}</Link>);
    }

    return links;
  }

  createPortfolioOptions = (loggedIn, portfolios) => {
    let links = [];
    for (let i = 0; i < portfolios.length; i++) {
      let classes = 
        location.pathname.includes(portfolios[i].toLowerCase()) ? 
          `option selected`
        : `option`;

      let url = location.pathname.includes(portfolios[i].toLowerCase()) ?
        `` : `${location.pathname}?portfolio=${portfolios[i].toLowerCase()}`;
      
      let onClick = location.pathname.includes(portfolios[i].toLowerCase()) ?
        null : (e) => { e.preventDefault(); };

      if (i == 0 && (location.pathname == "/user/"  ||  !location.pathname.includes("?"))) {
        url = "";
        classes += " selected";
        onClick = (e) => { e.preventDefault(); };
      }
      
      links.push(<Link className={classes} Url={url} onClick={onClick}>{portfolios[i]}</Link>);
    }

    // if (loggedIn)
    //   links = this.createUserHomeOptions(links);

    return links;
  }

  render() {
    const { size } = this.props;
    let path = location.pathname;
    return (
      <UserAuthSubscriber>
        {auth => (
          <ContentBlock className="options no-select" size={size}>
            {(path.includes("/artwork") || path == "/") && this.createHomeOptions(auth.state.authToken)}
            {path.includes("/user/") && this.createPortfolioOptions(auth.state.authToken, ["Portfolio"])}
            {/*<Link className="dropdown filter">All Media</Link>*/}
          </ContentBlock>
        )}
      </UserAuthSubscriber>
    );
  }
}

export { ArtworkFeedOptions };