import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import { ArtworkFeedOptions, ArtworkBlock, ArtworkGrid } from 'Sections';
import './ArtworkFeed.css';

import { ArtworkServiceProvider, ArtworkServiceSubscriber } from 'services';

class ArtworkFeed extends Component {
    constructor(props) {
      super(props);
    }

    state = { 
      Path: this.props.location.pathname,
      ContentSize: this.props.ContentSize ? Number.parseInt(this.props.ContentSize) : 10,
      Grid: null
    }

    static getDerivedStateFromProps(nextProps, prevState) {
      let state = { };
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
  
      return state;
    }

    componentDidMount() {
      if (this.state.Path !== this.props.location.pathname || this.state.Grid === null)
        this.setGridSize();
    }

    componentDidUpdate(prevProps) {
      if (this.props.location.pathname !== prevProps.location.pathname)
        this.setGridSize();
    }

    setGridSize = () => {
      if (window.location.pathname.includes("/artwork/latest"))
        this.setState({ Path: window.location.pathname, Grid: 1 })
      else
        this.setState({ Path: window.location.pathname, Grid: 2 })
    }

    render = () => {
      const { UserID, location } = this.props;
      const { ContentSize, Grid } = this.state;

      return (
        <ArtworkServiceProvider>
          <ArtworkServiceSubscriber>
            { artwork => (
              <ContentBlock className="artwork-feed" size={ContentSize}>
                <ArtworkFeedOptions/>
                <ArtworkGrid location={location} ContentSize={ContentSize} service={artwork} UserID={UserID} size={Grid}/>
              </ContentBlock>
            )}
          </ArtworkServiceSubscriber>
        </ArtworkServiceProvider>
      );
    }
}

export { ArtworkFeed };