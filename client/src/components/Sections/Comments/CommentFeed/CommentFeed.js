/*
ArtXperience Component
Comments
*/
import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import { Comment } from 'Sections';
import './CommentFeed.css';

class CommentFeed extends Component {
  constructor(props) {
    super(props);
  }

  state = { 
    ContentSize: this.props.ContentSize !== undefined ? Number.parseInt(this.props.ContentSize) : 10,
  }

  componentDidMount() { }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }
    
    return state;
  }

  render() {
    const { id, className, children, style } = this.props;
    const { ContentSize } = this.props;
    return (
      <ContentBlock className="comment-feed" size={ContentSize}>
        <h2>Comments</h2>
        <Comment/>
      </ContentBlock>
    );
  }
}

export { CommentFeed };