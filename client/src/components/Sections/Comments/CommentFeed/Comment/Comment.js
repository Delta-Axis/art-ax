import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import './Comment.css';

class Comment extends Component {
  constructor(props) {
    super(props);
  }

  state = { }

  componentDidMount() {  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    if (prevState) {
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
    }

    return state;
  }

  render() {
    return (
      <ContentBlock className="comment">
        <ContentBlock className="comment-content" style={{ textAlign: 'center', textTransform: 'capitalize' }}>
          feature not yet implemented
        </ContentBlock>
      </ContentBlock>
    );
  }
}

export { Comment };