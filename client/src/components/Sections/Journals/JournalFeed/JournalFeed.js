/*
ArtXperience Component
JournalFeed
*/
import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import { JournalEntry } from 'Sections';
import './JournalFeed.css';

import { JournalServiceProvider, JournalServiceSubscriber } from 'services';

class JournalFeed extends Component {
  constructor(props) {
    super(props)
  }

  state = { 
    ContentSize: this.props.ContentSize !== undefined ? Number.parseInt(this.props.ContentSize) : 10,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    if (prevState) {
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
    }

    return state;
  }

  renderJournals() {

  }

  render = () => {
    const { ContentSize } = this.state;
    return (
      <JournalServiceProvider>
        <JournalServiceSubscriber>
          { journal => (
            <ContentBlock className="journal-feed" size={ContentSize}>
              <h2>
                { 
                  location.pathname.includes("/user/") ? 
                    `Journal Entries` 
                  : `Journal Updates`
                }
              </h2>
              <JournalEntry service={journal}/>
            </ContentBlock>
          )}
        </JournalServiceSubscriber>
      </JournalServiceProvider>
    );
  }
}

export { JournalFeed };
