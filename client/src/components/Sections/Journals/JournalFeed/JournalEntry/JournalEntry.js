import React, { Component } from 'react';
import { ContentBlock } from 'Layout';
import './JournalEntry.css';

class JournalEntry extends Component {
  constructor(props) {
    super(props);
  }

  state = { }

  componentDidMount() {  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  render() {
    return (
      <ContentBlock className="journal-entry">
        <ContentBlock className="journal-entry-content" style={{ textAlign: 'center', textTransform: 'capitalize' }}>
          feature not yet implemented
        </ContentBlock>
      </ContentBlock>
    );
  }
}

export { JournalEntry };