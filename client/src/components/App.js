import React, { Component } from 'react';
import history from 'History';
import { Switch, Route } from 'react-router-dom';
import 'assets/css/App.css';
import 'assets/css/General.css';
import 'assets/css/Users.css';
import { ContentBody, NavBar, NavSection, NavLink } from 'Layout';
import { HomeFeed, UserProfile, ArtworkPost, NotFound, Registration, ArtworkSubmission } from 'Pages';
import { LoginButton } from 'Sections';


import { UserAuthSubscriber } from 'services';

class App extends Component {
  componentDidMount() { }
  
  render() {
    return (
      <UserAuthSubscriber>
        { auth => (
          <div className="App">
            <ContentBody>
          
              <NavBar>
                <NavSection align="left">
                  <NavLink Logo="/assets/images/logo.png" Url="/" PageName="ArtXperience"/>
                  { auth.state.authToken && !location.pathname.includes("/submit") && 
                    <button className="submit-artwork" onClick={() => history.push('/submit')}>Submit</button>
                  }
                </NavSection>
                <NavSection align="right">
                  { false && <NavLink Url="/sign-in" PageName="Sign In"/> }
                  { auth.state.authToken ?
                      <React.Fragment>
                        <NavLink Url={`/user/${auth.state._id}`}>{auth.state.username}</NavLink>
                        <NavLink PageName="Sign Out" onClick={(e) => { e.preventDefault(); auth.logout() }}/>
                      </React.Fragment>
                    : <React.Fragment>
                        <NavLink Url="/registration" PageName="Join Us"/>
                        <LoginButton auth={auth}/>
                      </React.Fragment>
                  }
                </NavSection>
              </NavBar>
              
              <Switch>
                <Route exact path="/" component={HomeFeed}/>  
                <Route path="/artwork" component={HomeFeed}/>
                <Route path="/registration" component={Registration}/>
                {!auth.state.authToken && <Route exact path="/user/" component={NotFound}/>}
                <Route path="/user/:id?" component={UserProfile}/>
                <Route exact path="/art/" component={NotFound}/>
                <Route path="/art/:id?" component={ArtworkPost} />
                {auth.state.authToken && <Route path="/submit" component={ArtworkSubmission}/>}
                <Route path="*" component={NotFound} />
              </Switch>
            </ContentBody>
          </div>
        )}
      </UserAuthSubscriber>
    );
  }
}

export default App;