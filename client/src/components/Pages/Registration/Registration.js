/*
ArtXperience Component
Login
*/
import React, { Component } from "react";
import { ContentArea, ContentBlock, FormGroup } from "Layout";
import "./Registration.css";

import { UserAuthSubscriber } from "services";

class Registration extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <UserAuthSubscriber>
        { auth => (
          <RegistrationView {...this.props} service={auth}/>
        )}
      </UserAuthSubscriber>
    )
  }
}

class RegistrationView extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    service: this.props.service ? this.props.service : null,
    firstname:'', 
    lastname:'',
    username:'',
    email:'', 
    confirmEmail:'',
    birthDate:'',
    gender:'',
    firstNameError:'',
    lastNameError:'',
    usernameError:'',
    emailError:'',
    confirmEmailError:'',
    passwordError:'',
    birthDateError:'',
    genderError:''
  }
  
  componentDidMount() {
    let page = "Join Us";
    if (document.title.includes(" | ")) {
      let title = document.title.split(" | ");
      title[title.length - 1] = page;
      document.title = title.join(" | ");
    } else {
      document.title = `${document.title} | ${page}`;
    }
  }
  
  handleFirstNameChange = (e) => {
    this.setState({ firstname: e.target.value }, () => { this.validateFirstName(); }); 
  }

  handleLastNameChange = (e) => {
    this.setState({ lastname: e.target.value }, () => { this.validateLastName(); });
  }

  handleUsernameChange = (e) => {
    this.setState({ username: e.target.value }, () => { this.validateUsername(); });
  }

  handleEmailChange = (e) => {
    this.setState({ email: e.target.value }, () => { this.validateEmail(); });
  }

  handleConfirmEmailChange = (e) => {
    this.setState({ confirmEmail: e.target.value }, () => { this.validateConfirmEmail(); })
  }

  handlePasswordChange = (e) => {
    this.validatePassword(e.target.value);
  }

  handleBirthDateChange = (e) => {
    this.setState({ birthDate: e.target.value }, () => { this.validateBirthDate(); });
  }
  
  handleGenderChange = (e) => {
    this.setState({ gender: e.target.value }, () => { this.validateGender(); })
  }

  validateFirstName = () => {
    const { firstname } = this.state;
    const firstnameRegex = /^[A-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)[^ ]*$/;

    const isValidFirstname = firstnameRegex.test(firstname);
    this.setState({
      firstNameError:
        isValidFirstname ? null : 
        'Must start with capital, no numbers or symbols and have no trailing spaces'
    });
  }

  validateLastName = () => {
    const { lastname } = this.state;
    const lastnameRegex = /^[A-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)[^ ]*$/;
    
    const isValidLastname = lastnameRegex.test(lastname);
    this.setState({
      lastNameError:
        isValidLastname ? null : 
        'Must start with capital, no numbers or symbols and have no trailing spaces'
    });
  }

  validateUsername = () => {
    const { username } = this.state;
    const usernameRegex = /^[a-zA-Z0-9_-]*$/;

    const isValidUsername = usernameRegex.test(username);
    this.setState({
      usernameError:
        isValidUsername ? null : 
        'An invalid username has been provided. No spaces or slashes ("/" or "\\")'
    });
  }

  validateEmail = () => {
    const { email } = this.state;
    const emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    const isValidEmail = emailRegex.test(email);
    this.setState({ emailError: isValidEmail ? null : 'Invalid Email Address' });
  }

  validateConfirmEmail = () => {
    const { confirmEmail, email } = this.state;

    const isValidConfirmEmail = confirmEmail === email;
    this.setState({
      confirmEmailError: isValidConfirmEmail ? null : 'Email must match the email above'
    });
  }

  validatePassword = (password) => {
    this.setState({ 
      passwordError: password.length > 7 ? null : 'Your password must be a minimum of 8 characters' 
    });
  }

  validateBirthDate = () => {
    const { birthDate } = this.state;
    const birthDateRegex = /^\d{1,2}\/\d{1,2}\/\d+$/;

    const isValidBirthDate = birthDateRegex.test(birthDate);
    this.setState({ birthDateError: isValidBirthDate ? null : 'Date of Birth must be MM/DD/YYYY format' });
  }

  validateGender = () => {
    const { gender } = this.state;
    
    this.setState({ genderError: gender ? null : 'Please select a gender option' });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { service } = this.state;
    const { firstname, lastname, username, email, birthDate, gender } = this.state;
    let password = e.target.password.value;
    const { firstNameError, lastNameError, usernameError, emailError, confirmEmailError, passwordError, birthDateError, genderError } = this.state;

    if (!firstNameError && !lastNameError
      && !usernameError && !emailError 
      && !confirmEmailError && !passwordError
      && !birthDateError && !genderError)
      if (e.target.agreement.checked)
        service.register(firstname, lastname, username, email, password, birthDate, gender);
      else
        this.setState({ agreementError: "You must agree to the terms of service to register" });
  }

  render() {
    return (
      <ContentArea footer={false}>
        <ContentBlock className="sign-up" size="5">
          <ContentBlock className="sign-up-banner">
            <div>Sign Up For Access To</div>
            <div className="ax-sign-up-branding">Art<span>Xperience</span></div>
          </ContentBlock>
          <ContentBlock className="sign-up-form">
            <form method="post" onSubmit={this.handleSubmit}>
              <div style={{ textAlign: "right" }}>All fields are required</div>

              <FormGroup>
                <label>First Name</label>
                <input required type="text" name="firstname" className="input-text-box"  placeholder="John" autoCapitalize="words"
                  value={this.state.firstname} onChange={this.handleFirstNameChange} onBlur={this.validateFirstName} />
                <div className="form-error-message">{this.state.firstNameError}</div>
              </FormGroup>

              <FormGroup>
                <label>Last Name</label>
                <input required type="text" name="lastname" className="input-text-box"  placeholder="Doe" autoCapitalize="words"
                  value={this.state.lastname} onChange={this.handleLastNameChange} onBlur={this.validateLastName} />
                <div className="form-error-message">{this.state.lastNameError}</div>
              </FormGroup>

              <FormGroup>
                <label>Username</label>
                <input required type="text" name="username" className="input-text-box" 
                  value={this.state.username} onChange={this.handleUsernameChange} onBlur={this.validateUsername} />
                <div className="form-error-message">{this.state.usernameError}</div>
              </FormGroup>

              <FormGroup>
                <label>Email Address</label>
                <input required type="text" name="email" className="input-text-box" placeholder="johndoe@email.com" 
                  value={this.state.email} onChange={this.handleEmailChange} onBlur={this.validateEmail} />
                <div className="form-error-message">{this.state.emailError}</div>
              </FormGroup>

              <FormGroup>
                <label>Confirm Email Address</label>
                <input required type="text" name="confirmEmail" className="input-text-box" 
                  value={this.state.confirmEmail} onChange={this.handleConfirmEmailChange} 
                  onBlur={this.validateConfirmEmail} onPaste={e => e.preventDefault()} autoComplete="no-complete"/>
                <div className="form-error-message">{this.state.confirmEmailError}</div>
              </FormGroup>

              <FormGroup>
                <label>Password</label>
                <input required type="password" name="password" className="input-text-box" 
                  value={this.state.password} onChange={this.handlePasswordChange} onBlur={e => this.validatePassword(e.target.value)} />
                <div className="form-error-message">{this.state.passwordError}</div>
              </FormGroup>

              <FormGroup>
                <label>Date of Birth</label>
                <input required type="text" name="birthDate" placeholder="MM/DD/YYYY" className="date-input" autoComplete="no-complete"
                  value={this.state.birthDate} onChange={this.handleBirthDateChange} onBlur={this.validateBirthDate} />
                <div className="form-error-message">{this.state.birthDateError}</div>
              </FormGroup>
              
              <FormGroup>
                <label>Gender</label>
                <select required className="gender-input" name="gender" defaultValue="" 
                  value={this.state.gender} onChange={this.handleGenderChange} onBlur={this.validateGender}>
                  <option value="" disabled>
                    Select Gender
                  </option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
                <div className="form-error-message">{this.state.genderError}</div>
              </FormGroup>
              
              <FormGroup>
                <input required type="checkbox" id="agreement" name="agreement" value="agree" />
                <label htmlFor="agreement">I agree to the Terms of Service</label>
              </FormGroup>
              { this.state.agreementError && <p>{this.state.agreementError}</p> }
              <input type="submit" value="JOIN THE EXPERIENCE" className="join-us" />
            </form>
          </ContentBlock>
        </ContentBlock>
      </ContentArea>
    );
  }
}

export { Registration };