/*
ArtXperience Component
HomeFeed
*/
import React, { Component } from 'react';
import { NavFooter, ContentArea, ContentBlock } from 'Layout';
import { FooterContent, ArtworkFeed, JournalFeed } from 'Sections';
import css from './HomeFeed.css';

import { UserAuthSubscriber } from 'services';

class HomeFeed extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<UserAuthSubscriber>
				{ auth => (
					<HomeFeedView {...this.props} auth={auth} />
				)}
			</UserAuthSubscriber>
		)
	}
}

class HomeFeedView extends Component {
	constructor(props) {
		super(props);
	}

	state = {
		auth: this.props.auth ? this.props.auth : null
	}

	componentDidMount() {
    let page = "Home";
    if (document.title.includes(" | ")) {
      let title = document.title.split(" | ");
      title[title.length - 1] = page;
      document.title = title.join(" | ");
    } else {
      document.title = `${document.title} | ${page}`;
    }
	}

	render() {
		const { location } = this.props;
		const { auth } = this.state;

		return (
			<ContentArea FooterContent={<FooterContent/>}>
				<ContentBlock>
					<header className="App-header">
						<div className="App-logo"><img src="/assets/images/logo.png" style={{ height: "100%", width: "auto"}} /></div>
						<div className="App-title"><h1>Welcome to ArtXperience</h1></div>
					</header>
				</ContentBlock>
				<ArtworkFeed key="home-artwork-feed" location={location} ContentSize={auth.state.authToken ? "7" : "10"}/>
				{ auth.state.authToken && <JournalFeed key="home-journal-feed" ContentSize="3"/> }
			</ContentArea>
		);
	}

}

export { HomeFeed };