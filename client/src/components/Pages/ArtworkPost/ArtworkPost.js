/*
ArtXperience Component
ArtworkPost
*/
import React, { Component } from 'react';
import { ContentArea, ContentBlock } from 'Layout';
import { FooterContent, ArtworkPostImage, ArtworkPostDetails } from 'Sections';
import './ArtworkPost.css';

import { ArtworkServiceProvider, ArtworkServiceSubscriber } from 'services';
import { Artwork as ArtworkModel } from 'models';

class ArtworkPost extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ArtworkServiceProvider>
        <ArtworkServiceSubscriber>
          { artwork => (
            <ArtworkPostView {...this.props} service={artwork}/>
          )}
        </ArtworkServiceSubscriber>
      </ArtworkServiceProvider>
    );
  }
}

class ArtworkPostView extends Component {
  constructor(props) {
    super(props);

    const { match } = this.props;
    const { service } = this.state;

    if (service)
      service.getArtwork(match.params.id);
  }

  state = { 
    service: this.props.service ? this.props.service : null,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    for (let key in nextProps) {
      if (prevState.hasOwnProperty(key)) {
        if (nextProps[key] !== prevState[key])
          state[key] = nextProps[key];
      }
    }

    return state;
  }

  render() {
    const { service } = this.state;
    let Artwork = service.state.artwork;

    return (
      Artwork._id ?
        <ContentArea FooterContent={<FooterContent/>}>
          <ContentBlock className="artwork-post d-flex">
            <ArtworkPostImage ContentSize="7" Image={`/artworks/${Artwork.filename}`}/>
            <ArtworkPostDetails ContentSize="3" Artwork={Artwork}/>
          </ContentBlock>
        </ContentArea>
      : null
    );
  }
}

export { ArtworkPost };