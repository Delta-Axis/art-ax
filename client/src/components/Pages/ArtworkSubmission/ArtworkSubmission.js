/*
ArtXpereience Component
ArtworkSubmission
*/
import React, { Component } from "react";
import { ContentArea, ContentBlock } from "Layout";
import { FooterContent } from 'Sections';
import "./ArtworkSubmission.css";
import axios from 'axios';

import { UserAuthSubscriber, ArtworkServiceProvider, ArtworkServiceSubscriber } from "services";

class ArtworkSubmission extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <UserAuthSubscriber>
        { auth => (
          <ArtworkServiceProvider>
            <ArtworkServiceSubscriber>
              { artwork => (
                <ArtworkSubmissionView  {...this.props} service={{ auth: auth, artwork: artwork }}/>
              )}
            </ArtworkServiceSubscriber>
          </ArtworkServiceProvider>
        )}
      </UserAuthSubscriber>
    )
  }
}

class ArtworkSubmissionView extends Component {
  fileReader = null;
  state = {
    service: this.props.service ? this.props.service : null,
    images: [],
  };

  validExtensions() {
    return ['.png', '.jpg', '.jpeg', '.pjpeg', '.jfif', '.pjp'];
  }

  static getDerivedStateFromProps(nextProps, prevState) { 
    return null;
  }

  componentDidMount() {
    let page = "Submission";
    if (document.title.includes(" | ")) {
      let title = document.title.split(" | ");
      title[title.length - 1] = page;
      document.title = title.join(" | ");
    } else {
      document.title = `${document.title} | ${page}`;
    }
	}

  handleFileRead = (e) => {
    const imageData = e.target.result;
    let { images } = this.state;
    images[images.length] = `url('${imageData}')`;
    this.setState({ images: images });
  }

  handleFileChange = (e) => {
    let files = e.target.files;
    if (files.length > 0) {
      let valid = false;
      for (let i = 0; i< files.length; i++) {
        let reader = new FileReader();
        // set reader onloadend event
        reader.onloadend = this.handleFileRead;
        
        // store file into variable for ease of typing
        let file = files[i];

        let fileLen = file.name.length;
        for (let ext of this.validExtensions()) {
          let extLen = ext.length;
          if (file.name.substr(fileLen - extLen, extLen).toLowerCase() == ext.toLowerCase()) {
            valid = true; break;
          }
        }

        if (!valid) {
          alert(`Sorry, the file type of ${file.name} is invalid.`);
          e.target.value = "";
          this.setState({ images: [] });
          return false;
        }

        reader.readAsDataURL(file);
      }

      return true;
    } else {
      return false;
    }
  }

  handleFileDrop = (e) => {
    this.handleFileChange(e);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { service: { auth, artwork } } = this.state;

    const data = new FormData();
    data.append('userId', auth.state._id);
    data.append('title', e.target["artwork-title"].value);
    data.append('description', e.target["artwork-description"].value);
    data.append('tags', e.target["artwork-tags"].value);
    data.append('role', auth.state.role);
    data.append('artwork-file', e.target["artwork-file"].files[0]);
    
    artwork.upload(data, auth.state.authToken);
  }

  render() {
    const { images } = this.state;

    return (
      <ContentArea FooterContent={<FooterContent/>}>
        <form className="w-10" onSubmit={this.handleSubmit}>
          <ContentBlock className="artwork-dropbox">
            <div className={`art-submission-box${images.length > 0 ? ` chosen` : ``}`}
              style={{ backgroundImage: images.length > 0 ? images[images.length-1] : null }}>
              <label className="art-submission-text">
                  <p className="no-margin">
                    Click to Add Your Artwork
                    <span className="subscript">(Or Drag to Add)</span>
                  </p>
                <input type="file" className="add-file" 
                  name="artwork-file" accept="image/png, image/jpeg" 
                  onDrop={this.handleFileDrop} 
                  onChange={this.handleFileChange}
                  required/>
              </label>
            </div>
          </ContentBlock>
          <ContentBlock className="artwork-form">
              <input type="text" className="artwork-title" 
                name="artwork-title" placeholder="Artwork Title" required/>
              
              <textarea type="text" className="artwork-description" 
                name="artwork-description" placeholder="Artwork Description" />
              
              <input type="text" className="artwork-tags"
                name="artwork-tags" placeholder="Tags #art #painting #sketch #drawing #pencil #etc" />
              
              <button type="submit" className="artwork-submit">Submit</button>
          </ContentBlock>
        </form>
      </ContentArea>
    );
  }
}

export { ArtworkSubmission };
