/*
ArtXperience Component
UserProfile
*/
import React, { Component } from 'react';
import { ContentArea, ContentBlock } from 'Layout';
import { FooterContent, UserBio, ArtworkFeed, JournalFeed } from 'Sections';
import css from './UserProfile.css';

import { UserAuthSubscriber, UserServiceProvider, UserServiceSubscriber } from 'services';

class UserProfile extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<UserAuthSubscriber>
				{	auth => (
					<UserServiceProvider>
						<UserServiceSubscriber>
							{ user => (<UserProfileView {...this.props} service={{ auth: auth, user: user }} />)}
						</UserServiceSubscriber>
					</UserServiceProvider>
				)}
			</UserAuthSubscriber>
		)
	}
}

class UserProfileView extends Component {
	constructor(props) {
		super(props);
	}

	state = {
		service: this.props.service ? 
			this.props.service 
		: {
				auth: null,
				user: null
		},
	}

	componentDidMount() {
		const { match } = this.props;
		const { service: { auth, user } } = this.state;

		if (user) {
			if (match.params.id)
				user.getUser(match.params.id);
			else
				user.getUser(auth.state._id);
		}
	}

	componentDidUpdate(prevProps) {
		if (this.props.location.pathname !== prevProps.location.pathname) {
			const { match } = this.props;
			const { service: { auth, user } } = this.state;

			if (user) {
				if (match.params.id)
					user.getUser(match.params.id);
				else
					user.getUser(auth.state._id);
			}
		}
	}

	render() {
		const { service: { auth, user } } = this.state;
		let User = user.state.user;

		return (
			<ContentArea FooterContent={<FooterContent/>}>
				{ User._id &&
					<React.Fragment>
						<UserBio User={User}/>
						<ArtworkFeed key="user-artwork-feed" location={location} UserID={User._id} ContentSize="7"/>
						<JournalFeed key="user-journal-feed" UserID={User._id} ContentSize="3"/>
					</React.Fragment>
				}
			</ContentArea>
		);
	}
}

export { UserProfile }