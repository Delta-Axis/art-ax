const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const comment = require('../../models/comment');
const { Auth } = require('../../utils');

router.use((req, res, next) => {
  next();
});

router.get('', (req, res, next) => {
  res.contentType("application/json");
  console.log('GET: Comments Lists');
  comment.find({}, (err, comments) => {
    if (err) 
      throw err;
    res.send(JSON.stringify(comments));
  });
});

router.get('/:commentId', (req, res, next) => {
  res.contentType("application/json");
  if (req.params.commentId) {
    console.log('GET: comment by id:' + req.params.commentId);
    
    comment.findOne({ _id: req.params.commentId }, (err, comment) => {
      if (err)  throw err;
      
      if (comment == null)
        res.send(JSON.stringify({ message: `The comment with id "${req.params.commentId}" does not exist in the database` }));
      else
        res.send(JSON.stringify(comment));
    });

  } else {
    res.send(JSON.stringify({ message: "No comment returned due to having no queries" }));
  }
})

router.post('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token)) {
      if (values && Object.keys(values).length > 0 && values.commentId) {
        console.log('CREATE: Comment: ' + values.commentId);
        let comment = Comment(values);
        comment.save((err, doc) => {
          if (err)
            throw err;

          console.log(`Created new comment: ${values.commentId}`);
          res.send(JSON.stringify(doc));
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid CREATE Query for Comment" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No Token Given" }));
  }
})

router.put('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token) 
    || (values.type && values.type === "invite")) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('UPDATE: Comment by id: ' + values._id);

        Comment.findByIdAndUpdate(values._id, values, (err, comment) => {
          if (err) throw err;

          if (comment == null)
            res.send(JSON.stringify({ message: "Invalid UPDATE Query for Comment" }));
          else {
            console.log(`Updated ${comment.commentId} from Comments collection`);
            res.send(JSON.stringify(comment));
          }
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid UPDATE Query for Comment" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No token Given" }));
  }
})

router.delete('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token)) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('DELETE: Comment by id:' + values._id);

        Comment.findByIdAndDelete(values._id, (err) => {
          console.log(`Deleted ${values.commentId} from Comments collection`);
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid DELETE Query for Comment" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No Token Given" }));
  }
});

module.exports = router;