const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const Journal = require('../../models/journal');
const { Auth } = require('../../utils');

router.use((req, res, next) => {
  next();
});

router.get('', (req, res, next) => {
  res.contentType("application/json");
  console.log('GET: Journals Lists');
  Journal.find({}, (err, journals) => {
    if (err) 
      throw err;
    res.send(JSON.stringify(journals));
  });
});

router.get('/:journalId', (req, res, next) => {
  res.contentType("application/json");
  if (req.params.journalId) {
    console.log('GET: journal by id:' + req.params.journalId);
    
    Journal.findOne({ _id: req.params.journalId }, (err, journal) => {
      if (err)  throw err;
      
      if (journal == null)
        res.send(JSON.stringify({ message: `The journal with id "${req.params.journalId}" does not exist in the database` }));
      else
        res.send(JSON.stringify(journal));
    });

  } else {
    res.send(JSON.stringify({ message: "No journal returned due to having no queries" }));
  }
})

router.post('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token)) {
      if (values && Object.keys(values).length > 0 && values.journalId) {
        console.log('CREATE: Journal: ' + values.journalId);
        let journal = Journal(values);
        journal.save((err, doc) => {
          if (err)
            throw err;

          console.log(`Created new journal: ${values.journalId}`);
          res.send(JSON.stringify(doc));
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid CREATE Query for Journal" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No Token Given" }));
  }
})

router.put('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token) 
    || (values.type && values.type === "invite")) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('UPDATE: Journal by id: ' + values._id);

        Journal.findByIdAndUpdate(values._id, values, (err, journal) => {
          if (err) throw err;

          if (journal == null)
            res.send(JSON.stringify({ message: "Invalid UPDATE Query for Journal" }));
          else {
            console.log(`Updated ${journal.journalId} from Journals collection`);
            res.send(JSON.stringify(journal));
          }
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid UPDATE Query for Journal" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No token Given" }));
  }
})

router.delete('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token)) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('DELETE: Journal by id:' + values._id);

        Journal.findByIdAndDelete(values._id, (err) => {
          console.log(`Deleted ${values.journalId} from Journals collection`);
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid DELETE Query for Journal" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No Token Given" }));
  }
});

module.exports = router;