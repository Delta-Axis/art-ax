// server/routes/api/user.js
const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const bodyParser = require('body-parser');

const { Auth } = require('../../utils');

router.use((req, res, next) => {
  next();
});

router.get('', (req, res, next) => {
  res.contentType("application/json");
  console.log('GET: Retrieve all Users');
  User.find({}, (err, users) => {
    if (err) 
      throw err;
    res.send(JSON.stringify(users));
  });
});

const UserRetrievalResponse = (user, param) => {
  if (user == undefined || user == null)
    return JSON.stringify({ message: `The user "${param}" does not exist in the database` });
  else {
    userInfo = JSON.parse(JSON.stringify(user));
    delete userInfo.password;
    delete userInfo.user_role;

    return JSON.stringify(userInfo);
  }
}

router.get('/:userId', (req, res, next) => {
  res.contentType("application/json");
  if (req.params.userId) {
    console.log('GET: user by id:' + req.params.userId);
    
    User.findOne({ userId: req.params.userId }, (err, user) => {
      if (err)  throw err;
      
      res.send(
        UserRetrievalResponse(user, req.params.userId)
      );
    });

  } else {
    res.send(JSON.stringify({ message: "No user returned due to having no queries" }));
  }
})

router.post('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values && Object.keys(values).length > 0) {
    if (values.userId) {
      console.log('POST: Retrieve User By ID: ' + values.userId);
      User.findById(values.userId, (err, user) => {
        if (err) throw err;
    
        res.send(
          UserRetrievalResponse(user, values.userId)
        );
      });
    } else if (values.username) {
      User.findOne({ username: values.username }, (err, user) => {
        if (err) throw err;

        res.send(
          UserRetrievalResponse(user, values.username)
        );
      });
    } else {
      res.send(JSON.stringify({ message: "Invalid POST API Query for User" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid POST API Query for User" }));
  }
})

router.put('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token) 
    || (values.type && values.type === "invite")) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('UPDATE: User by id: ' + values._id);

        User.findByIdAndUpdate(values._id, values, (err, user) => {
          if (err) throw err;

          if (user == null)
            res.send(JSON.stringify({ message: "Invalid UPDATE Query for User" }));
          else {
            console.log(`Updated ${user.userId} from users collection`);
            res.send(JSON.stringify(user));
          }
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid UPDATE Query for User" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No token Given" }));
  }
})

router.delete('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token)) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('DELETE: User by id:' + values._id);

        User.findByIdAndDelete(values._id, (err) => {
          console.log(`Deleted ${values.userId} from users collection`);
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid DELETE Query for User" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No Token Given" }));
  }
});

module.exports = router;