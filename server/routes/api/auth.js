const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const User = require('../../models/user');
const { Auth } = require('../../utils');
const bcrypt = require('bcrypt');

/* User Authentication Route */
router.post('', (req, res, next) => {
  res.contentType("application/json");
  let authInfo = null;
  User.findOne({ username: new RegExp(`^${req.body.username}$`, 'i') }, (err, account) => {
    if (err) throw err;

    if (account == undefined || account == null) {
      res.send(JSON.stringify({ message: "Invalid Credentials" }));
    } else {
      bcrypt.compare(req.body.password, account.password, (err, match) => {
        if (err) throw err;
        
        if (match) {
          authInfo = {
            _id: account._id,
            username: account.username,
            role: account.user_role
          };
    
          let payload = { "_id": account._id, "role": account.user_role }
          let authType = account.user_role === "admin" ? 1 : 0;
          authInfo.authToken = Auth.generateToken(payload, authType);
          res.send(JSON.stringify(authInfo));
          console.log(`[EVENT] ${authInfo.username} has logged in.`);
        } else {
          res.send(JSON.stringify({ message: "Invalid Credentials" }));
        }
      });
    }
  });
});

router.post('/create', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values && Object.keys(values).length > 0 && values.username) {
    console.log('CREATE: account with username: ' + values.username);
    bcrypt.hash(values.password, 10, (err, hash) => {
      if (err) throw err;

      values.password = hash;
      let user = User(values);
      user.save((err, user) => {
        if (err)
          throw err;

        console.log(`Created new account: ${values.username}`);
        res.send(JSON.stringify({ message: "Registration Successful", registered: true }));
      });
    });
  } else {
    res.send(JSON.stringify({ message: "Invalid CREATE Query for Account", registered: false }));
  }
});

module.exports = router;