const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer = require('multer');
const fs = require('fs');

const Artwork = require('../../models/Artwork');
const { Auth } = require('../../utils');

router.use((req, res, next) => {
  next();
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null,  __dirname + '../../../uploads/')
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' +file.originalname)
  }
})

const upload = multer({ storage: storage });

router.post('/upload', (req, res, next) => {
  let ul = upload.single('artwork-file');
  ul(req, res, (err) => {
    res.contentType("application/json");
    if (err) {
      console.log(err);
      res.send(JSON.stringify({ message: "An unknown error has occurred", uploaded: false }));
    } else {
      let values = req.body;
      if (values) {
        let authType = values.role === "admin" ? 1 : 0; 
        if (Auth.verify(values.token, authType)) {
          if (values && Object.keys(values).length > 0 && values.userId) {
            values._id = mongoose.Types.ObjectId();
            values.userId = mongoose.Types.ObjectId(values.userId);
            let fileNameSplit = req.file.filename.split(".");
            let fileType = fileNameSplit[fileNameSplit.length-1];
            values.filename = `${values._id}.${fileType}`;

            let artwork = Artwork(values);

            artwork.save((error, artwork) => {
              if (error) {
                console.log(error);
                res.end(JSON.stringify({ message: "Upload failed", uploaded: false }));
              }
              
              if (artwork === undefined || artwork === null) {
                res.send(JSON.stringify({ message: "Upload failed", uploaded: false }));
              } else {
                console.log(`[EVENT]: User with id ${values.userId} initialised upload`);
                /** When using the "single"
                data come in "req.file" regardless of the attribute "name". **/
                let tmp_path = req.file.path;

                /** The original name of the uploaded file
                    stored in the variable "originalname". **/
                let target_path = `client/public/artworks/${values.filename}`;

                /** A better way to copy the uploaded file. **/
                let src = fs.createReadStream(tmp_path);
                let dest = fs.createWriteStream(target_path);

                src.on('end', function () {
                  fs.unlink(tmp_path, function () {});
                });

                let file = src.pipe(dest);
                file.on('finish', function () {
                  if (fs.existsSync(dest.path)) {
                    console.log(`[EVENT] User with id ${values.userId} upload successful | Artwork ID: ${values._id}`);
                    res.send(JSON.stringify({ message: "Upload successful", uploaded: true, artwork: artwork }));
                  } else {
                    console.log(`[EVENT] User with id ${values.userId} upload failed`);
                    res.send(JSON.stringify({ message: "Upload failed", uploaded: false }));
                  }
                })
              }
            });
          } else {
            res.send(JSON.stringify({ message: "Upload failed", uploaded: false }));
          }
        } else {
          res.send(JSON.stringify({ message: "Invalid User Token", uploaded: false }));
        }
      } else {
        res.send(JSON.stringify({ message: "Invalid User Token", uploaded: false }));
      }
    }
  })
})

router.get('', (req, res, next) => {
  res.contentType("application/json");
  console.log('GET: Retrieve all Artworks');
  Artwork.find({}, (err, artworks) => {
    if (err) 
      throw err;

    res.send(JSON.stringify(artworks));
  });
});

const ArtworkRetrievalResponse = (artwork, param) => {
  if (artwork == undefined || artwork == null) {
    return JSON.stringify({ message: `The artwork "${param}" does not exist in the database` });
  } else {
    artworkInfo = JSON.parse(JSON.stringify(artwork));

    return JSON.stringify(artworkInfo);
  }
}

router.get('/:artworkId', (req, res, next) => {
  res.contentType("application/json");
  if (req.params.artworkId) {
    console.log('GET: artwork by id:' + req.params.artworkId);
    
    Artwork.findById(req.params.artworkId, (err, artwork) => {
      if (err)  throw err;
      
      res.send(
        ArtworkRetrievalResponse(artwork, req.params.artworkId)
      );
    });

  } else {
    res.send(JSON.stringify({ message: "No artwork returned due to having no queries" }));
  }
})

router.post('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values.artworkId) {
    console.log('POST: Retrieve Artwork by ID: ' + values.artworkId);
    Artwork.findById(values.artworkId, (err, artwork) => {
      if (err)
        throw err;

      res.send(ArtworkRetrievalResponse(artwork, values.artworkId));
    });
  } else if (values.userId) {
    console.log('POST: Retrieve Artwork by User ID: ' + values.userId);
    Artwork.find({ userId: values.userId }, (err, artworks) => {
      if (err)
        throw err;
      
      res.send(JSON.stringify(artworks));
    });
  } else {
    res.send(JSON.stringify({ message: "Invalid POST API Query for Artwork" }));
  }
})

router.put('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token) 
    || (values.type && values.type === "invite")) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('UPDATE: Artwork by id: ' + values._id);

        Artwork.findByIdAndUpdate(values._id, values, (err, artwork) => {
          if (err) throw err;

          if (artwork == null)
            res.send(JSON.stringify({ message: "Invalid UPDATE Query for Artwork" }));
          else {
            console.log(`Updated ${artwork.artworkId} from arts collection`);
            res.send(JSON.stringify(artwork));
          }
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid UPDATE Query for Artwork" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No token Given" }));
  }
})

router.delete('', (req, res, next) => {
  res.contentType("application/json");
  let values = req.body;
  if (values) {
    if (Auth.verify(values.token)) {
      if (values && Object.keys(values).length > 0 && values._id) {
        console.log('DELETE: Artwork by id:' + values._id);

        Artwork.findByIdAndDelete(values._id, (err) => {
          console.log(`Deleted ${values.artworkId} from arts collection`);
        });
      } else {
        res.send(JSON.stringify({ message: "Invalid DELETE Query for Artwork" }));
      }
    } else {
      res.send(JSON.stringify({ message: "Invalid Authorization Token" }));
    }
  } else {
    res.send(JSON.stringify({ message: "Invalid Authorization, No Token Given" }));
  }
});

module.exports = router;