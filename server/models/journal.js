const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const journalSchema = new Schema({
  title: String,
  content: String,
  userId: ObjectId,
  datePosted: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('journal', journalSchema);