const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const artworkSchema = new Schema({
  userId: ObjectId,
  title: String,
  description: String,
  tags: String,
  filename: String,
  datePosted: {
    type: Date,
    default: new Date()
  },
  lastModified: {
    type: Date,
    default: new Date()
  },
});

module.exports = mongoose.model('artwork', artworkSchema);