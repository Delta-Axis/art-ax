const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const commentSchema = new Schema({
  artworkId: Number,
  userId: ObjectId,
  comment: String,
  datePosted: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('comments', commentSchema);