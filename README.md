# ArtXperience by Delta Axis ©
## Team Members
| Role | Name |
| -------- | -------- |
| Team Lead / Lead developer  | Jullian Anthony Sy-Lucero  |
| Backend API/DB Developer | Jacky Phunh |
| Frontend Developer | Jarone Rodney |
| Frontend Developer | Charles Kenn Santiago |
## Project Setup
### Initial Setup
Before getting started... please run `npm install -g yarn` (or if on macOS or Linux make sure to run it with `sudo`)
1. Run `yarn install` / `yarn` (however, its possible `npm install` would work, but I wouldn't recommend it. - Jullian)
### Running the server
So, there really isn't much for server running, here are the steps:
1. Run `yarn start:dev` (or `npm run start:dev` if you don't want to use `yarn`)

## Site Preview
#### User Home Page
![User Home Page](https://i.imgur.com/qrZlWoW.png)
#### User Profile
![User Profile](https://i.imgur.com/dVf4yhE.png)
#### User Artwork Post
![User Artwork Post](https://i.imgur.com/PnoLqQ9.png)
