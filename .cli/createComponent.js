var { execSync } = require('child_process');
var path = require('path');

const componentName = process.argv[2] || "ReactComponent";

execSync(`npm i -g yarn`);
execSync(`cd ${path.join(__dirname, "/create-component/")} && yarn`);
execSync(`cd ${path.join(__dirname, "../client/components/")} && node ${path.join(__dirname, "/create-component/")} ${componentName} --css`, { stdio: [0, 1, 2] });